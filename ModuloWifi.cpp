#include "ModuloWifi.h"
#define EMPTY ""

ModuloWifi::ModuloWifi() : _headers(EMPTY),
  _sizeHeaders(0),
  //_commandResponse (EMPTY),
  _nToken(EMPTY)
{
}

ModuloWifi::~ModuloWifi()
{
}

void ModuloWifi::Add_header(String name, String value)
{
  _headers += name;
  _headers += ":";
  _headers += value;
  _headers += "\n";
  _sizeHeaders++;
}

String
ModuloWifi::Sync_data(String url, size_t size, String tipo)
{
  int httpCode = 0;
  String commandResponse = "ERROR";
  
  Serial.print("tamaño payload: ");
  Serial.println (size);
  
  WiFiClient client;

  /*if (fingerprint.length() > 0)
    {
    int comas = tools.Contar_caracter(',', fingerprint);
    uint8_t vectorFingerprint[comas + 1];
    String cadena = fingerprint;
    for (int i = 0; i < sizeof(vectorFingerprint) / sizeof(vectorFingerprint[0]); i++)
    {
      int index = cadena.indexOf(',');
      vectorFingerprint[i] = cadena.substring(0, index).toInt();
      cadena = cadena.substring(index + 1);
    }
    client->setFingerprint(vectorFingerprint);
    }
    else
    {
    client->setInsecure();
    }*/

  if (http.begin(client, url))
  {
    for (int i = 0; i < _sizeHeaders; i++)
    {
      // Serial.println (headers);
      String _header = _headers.substring(0, _headers.indexOf('\n'));
      _headers.remove(0, _headers.indexOf('\n') + 1);
      String _name = _header.substring(0, _header.indexOf(':'));
      _header.remove(0, _header.indexOf(':') + 1);
      String _value = _header.substring(0, _header.indexOf('\n'));

      http.addHeader(_name, _value);
    }
    _sizeHeaders = 0;
    _headers = "";

    http.setAuthorization(_nToken.c_str());

    // start connection and send HTTP header*/
    if (size > 0)
    {
      httpCode = http.sendRequest(tipo.c_str(), size);
    }
    else
    {
      httpCode = http.sendRequest(tipo.c_str());
    }

    // httpCode will be negative on error
    if (httpCode > 0)
    {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("WIFI: HTTP code -%d- \n", httpCode);
      Serial.println();

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == 201)
      {
        String payload = "#";
        payload += http.getString();
        payload += "#";
        Serial.print("WIFI: Recibi trama: ");
        Serial.println(payload);
        commandResponse = "OK";
      }
      else
      {
        commandResponse = "ERROR";
      }
    }
    else
    {
      Serial.printf("WIFI: HTTPS GET... failed, error: -%s\n-", http.errorToString(httpCode).c_str());
      commandResponse = "ERROR";
    }

    http.end();
  }
  else
  {
    commandResponse = "ERROR";
  }
  return commandResponse;
}

String
ModuloWifi::Sync_data(String url, size_t size, String fingerprint, String tipo)
{
  int httpCode = 0;
  String commandResponse = "ERROR";

  Serial.print("tamaño payload: ");
  Serial.println (size);

  std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);

  if (fingerprint.length() > 0)
  {
    int comas = tools.Contar_caracter(',', fingerprint);
    uint8_t vectorFingerprint[comas + 1];
    String cadena = fingerprint;
    for (int i = 0; i < sizeof(vectorFingerprint) / sizeof(vectorFingerprint[0]); i++)
    {
      int index = cadena.indexOf(',');
      vectorFingerprint[i] = cadena.substring(0, index).toInt();
      cadena = cadena.substring(index + 1);
    }
    client->setFingerprint(vectorFingerprint);
  }
  else
  {
    client->setInsecure();
  }

  if (https.begin(*client, url))
  {
    for (int i = 0; i < _sizeHeaders; i++)
    {
      // Serial.println (headers);
      String _header = _headers.substring(0, _headers.indexOf('\n'));
      _headers.remove(0, _headers.indexOf('\n') + 1);
      String _name = _header.substring(0, _header.indexOf(':'));
      _header.remove(0, _header.indexOf(':') + 1);
      String _value = _header.substring(0, _header.indexOf('\n'));

      https.addHeader(_name, _value);
    }
    _sizeHeaders = 0;
    _headers = "";

    https.setAuthorization(_nToken.c_str());

    // start connection and send HTTP header*/
    if (size > 0)
    {
      httpCode = https.sendRequest(tipo.c_str(), size);
    }
    else
    {
      httpCode = https.sendRequest(tipo.c_str());
    }

    // httpCode will be negative on error
    if (httpCode > 0)
    {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("WIFI: HTTPS code -%d- \n", httpCode);
      Serial.println();

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == 201)
      {
        String payload = "#";
        payload += https.getString();
        payload += "#";
        Serial.print("WIFI: Recibi trama: ");
        Serial.println(payload);
        commandResponse = "OK";
      }
      else
      {
        commandResponse = "ERROR";
      }
    }
    else
    {
      Serial.printf("WIFI: HTTPS GET... failed, error: -%s\n-", https.errorToString(httpCode).c_str());
      commandResponse = "ERROR";
    }

    https.end();
  }
  else
  {
    commandResponse = "ERROR";
  }
  return commandResponse;
}

void ModuloWifi::Set_token(String nToken)
{
  _nToken = nToken;
}

bool ModuloWifi::Add_AP(String ssid[], String psw[], byte len)
{
  WiFi.mode(WIFI_STA);
  for (int i = 0; i < len; i++)
  {
    if (!WiFiMulti.addAP(ssid[i].c_str(), psw[i].c_str()))
    {
      return false;
    }
  }
  return true;
}

bool ModuloWifi::Connect_to_AP()
{
  if ((WiFiMulti.run() == WL_CONNECTED))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ModuloWifi::Disconnect_to_AP()
{
  if (WiFi.disconnect())
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool FTP::Start_transfer(String user, String psw, String server, String path)
{
  bool result = false;
  if (_moduloWifi.Connect_to_AP())
  {
    Serial.print("*server*: ");
    Serial.println(server);

    FTPClient::ServerInfo ftpServerInfo(user, psw, server);

    if (LittleFS.begin())
    {
      _ftpClient.begin(ftpServerInfo);
      if (Transfer(path))
      {
        result = true;
      }
      else
      {
        result = false;
      }
    }
    else
    {
      result = false;
    }
  }
  else
  {
    result = false;
  }
  return result;
}

bool FTP::Transfer(String path)
{
  unsigned long _starTime = millis();
  bool transferComplete = false;
  bool transferStarted = false;

  do
  {
    _ftpClient.handleFTP();
    delay(10);

    if (!transferStarted)
    {
      _ftpClient.transfer(path, path, FTPClient::FTP_GET_NONBLOCKING);
      transferStarted = true;
    }
    const FTPClient::Status &r = _ftpClient.check();
    if (r.result == FTPClient::OK)
    {
      Serial.println("transfer completed");
      transferComplete = true;
      transferStarted = false;
    }
    else if (r.result == FTPClient::ERROR)
    {
      Serial.println("transfer failed");
      transferComplete = false;
      transferStarted = false;
    }
  } while (millis() - _starTime < _timeOut && !transferComplete);
  return transferComplete;
}
