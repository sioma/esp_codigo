#include "Tools.h"

Tools::Tools()
{
}

Tools::~Tools()
{
}

int
Tools::Contar_caracter(char caracter, String cadena) //Recibo como parametros un caracter y una cadena
{
  int contador = 0;
  for (int i = 0; i <= cadena.length(); i++)
  { //Recorro la cadena y cuento las veces que se encuentra el caracter
    if (cadena[i] == caracter)
    {
      contador++;
    }
  }
  return contador; //Retorno el contador
}

uint8_t
Tools::XORChecksum8(const byte * data, size_t dataLength)
{
  uint8_t value = 0;
  for (size_t i = 0; i < dataLength; i++)
  {
    value ^= (uint8_t)data[i];
  }
  return value;
}


String
Tools::decToHex(byte decValue, byte desiredStringLength) {

  String hexString = String(decValue, HEX);
  while (hexString.length() < desiredStringLength) hexString = "0" + hexString;

  return hexString;
}


unsigned int
Tools::hexToDec(String hexString) {

  unsigned int decValue = 0;
  int nextInt;

  for (int i = 0; i < hexString.length(); i++) {

    nextInt = int(hexString.charAt(i));
    if (nextInt >= 48 && nextInt <= 57) nextInt = map(nextInt, 48, 57, 0, 9);
    if (nextInt >= 65 && nextInt <= 70) nextInt = map(nextInt, 65, 70, 10, 15);
    if (nextInt >= 97 && nextInt <= 102) nextInt = map(nextInt, 97, 102, 10, 15);
    nextInt = constrain(nextInt, 0, 15);

    decValue = (decValue * 16) + nextInt;
  }

  return decValue;
}

bool
Tools::Check_integrity (String data, String * pt)
{
  String checksumRecibido = data.substring(data.length() - 2, data.length());
  data.remove (data.length() - 3, data.length());

  *pt = data;

  byte dataByte[data.length()];

  for (int i = 0; i < data.length(); i++)
  {
    dataByte[i] = data[i];
  }

  uint8_t checksumCalculado = XORChecksum8(dataByte, data.length());
  String checksumCalculadoHex = decToHex(checksumCalculado);

  if (checksumRecibido == checksumCalculadoHex) {
    return true;
  } else {
    return false;
  }
}


String 
Tools::Get_val_from_json(String json, String nombre)
{
  int index = json.indexOf(nombre);
  if (index != -1)
  {
    String valor = json.substring(index + nombre.length() + 3);
    valor = valor.substring(0, valor.indexOf('"'));
    return valor;
  } else
  {
    return "";
  }
}
