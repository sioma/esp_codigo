#ifndef _Tools_H
#define _Tools_H

#include <Arduino.h>

class Tools
{
  public:
    Tools ();
    virtual ~Tools();
    int Contar_caracter (char caracter, String cadena);
    uint8_t XORChecksum8(const byte * data, size_t dataLength);
    bool Check_integrity (String data, String * pt);
    String decToHex(uint8_t decValue, byte desiredStringLength = 2);
    unsigned int hexToDec(String hexString);
    String Get_val_from_json(String json, String nombre);

  private:

};

#endif /*_Tools_H*/
