#ifndef _ModuloWIFI_H
#define _ModuloWIFI_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <WiFiClientSecureBearSSL.h>
#include <LittleFS.h>
#include <FTPClient.h>
#include "Tools.h"


class ModuloWifi
{
  public:
    Tools tools;
    ESP8266WiFiMulti WiFiMulti;
    HTTPClient https;
    HTTPClient http;
    ModuloWifi ();
    virtual ~ModuloWifi();

    void Add_header (String name, String value);
    String Sync_data (String url, size_t size, String tipo);
    String Sync_data (String url, size_t size, String fingerprint, String tipo);
    void Set_token (String nToken);
    bool Add_AP(String ssid[], String psw[], byte len);
    bool Connect_to_AP ();
    bool Disconnect_to_AP ();



  private:
    String _headers;
    int _sizeHeaders;
    String _nToken;

};

class FTP
{
  public:
    FTP (ModuloWifi& moduloWifi, FTPClient& ftpCLient): _moduloWifi(moduloWifi), _ftpClient(ftpCLient), _timeOut(60000) {}
    virtual ~FTP() {}
    bool Start_transfer (String user, String psw, String server, String path);
    bool Transfer(String path);

  private:
    ModuloWifi& _moduloWifi;
    FTPClient& _ftpClient;
    const int _timeOut;


};
#endif  /*_ModuloWIFI_H*/
