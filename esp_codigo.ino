#include <Arduino.h>
#include "ModuloWifi.h"
#include "Samba.h"
#include "Device.h"
#include "Flasher.h"

#define STATUS_MICRO "status.txt"
#define STATUS_SSL "sslstatus.txt"
#define RECOVERY "recovery.bin"
#define PIN_RESET_MICRO 12
#define BAUD_RATE_PROGRAMER 115200
#define OFF_SET 0x4000

class BossaObserver : public FlasherObserver
{
  public:
    BossaObserver() : _lastTicks(-1) {}
    virtual ~BossaObserver() {}

    virtual void onStatus(const char *message, ...);
    virtual void onProgress(int num, int div);

  private:
    int _lastTicks;
};

void BossaObserver::onStatus(const char *message, ...)
{
  va_list ap;

  va_start(ap, message);
  vprintf(message, ap);
  va_end(ap);
}

void BossaObserver::onProgress(int num, int div)
{
  printf("Ingrese a onProgress...");
  int ticks;
  int bars = 30;

  ticks = num * bars / div;

  if (ticks == _lastTicks)
    return;

  printf("\r[");
  while (ticks-- > 0)
  {
    putchar('=');
    bars--;
  }
  while (bars-- > 0)
  {
    putchar(' ');
  }
  printf("] %d%% (%d/%d pages)", num * 100 / div, num, div);
  fflush(stdout);

  _lastTicks = 0;
}

enum actions
{
  addWifi,
  connectWifi,
  disconnectWifi,
  newHeader,
  serverConnection,
  tokenAuthorization,
  show,
  ftpTransfer,
  program,
  resetU,
  dResetU,
  insecure,
  macAddress,
  wait
};

actions action = show;
ModuloWifi wifi;
FTPClient ftpClient(LittleFS);
FTP ftp(wifi, ftpClient);
Tools tools;
Samba samba;

bool commandReceived = false;
String result = "";
String data = "";
String *ptData = &data;
size_t size = 1024;

void setup()
{
  Serial.setRxBufferSize(size);
  Serial.begin(9600);
}

void loop()
{

  if (action == show)
  {
    if (Check_status_micro())
    {
      Serial.println();
      Serial.println("WIFI: Waiting for commands...");
      action = wait;
    }
    else
    {
      action = program;
      data = RECOVERY;
    }
  }
  else if (action == wait)
  {
    if (commandReceived)
    {
      Serial.print("WIFI: ");
      Serial.println(result);
      result = "";
      commandReceived = false;
      action = show;
    }

    if (Serial.available())
    {
      char c = Serial.read();
      Serial.print (String(c));
      if (c == 'A')
      {
        action = addWifi;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'C')
      {
        action = connectWifi;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'D')
      {
        action = disconnectWifi;
      }
      else if (c == 'S')
      {
        action = serverConnection;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'N')
      {
        action = newHeader;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'T')
      {
        action = tokenAuthorization;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'F')
      {
        action = ftpTransfer;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'P')
      {
        action = program;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'I')
      {
        action = insecure;
        data = Serial.readStringUntil('\n');
        data.trim();
      }
      else if (c == 'M')
      {
        action = macAddress;
      }
      /*
        else if (c == 'R')
        {
        action = dResetU;
        }
        else if (c == 'r')
        {
        action = resetU;
        }*/
    }
    if (data != "")
    {
      if (!tools.Check_integrity(data, ptData))
      {
        data = "";
        action = show;
        Serial.println("WIFI: Los datos estas corruptos... ");
      }
    }
  }
  else if (action == addWifi)
  {
    Add_wifi(data);
    data = "";
    action = wait;
  }
  else if (action == connectWifi)
  {
    Connect_to_wifi(data);
    data = "";
    action = wait;
  }
  else if (action == disconnectWifi)
  {
    if (wifi.Disconnect_to_AP())
    {
      result = "OK";
    }
    else
    {
      result = "ERROR";
    }
    action = wait;
  }
  else if (action == macAddress)
  {
    commandReceived = true;
    action = wait;
    byte mac[6];
    WiFi.macAddress(mac);
    result = "MAC: ";
    for (int i = 0; i < 6; i++)
    {
      result += tools.decToHex(mac[i], 2);
      if (i < 5)
      {
        result += ":";
      }
    }
  }
  else if (action == serverConnection)
  {
    String estadoSSL = Check_status_ssl();
    if (estadoSSL.indexOf("SSL_OFF") >= 0)
    {
      No_SSL_protocol(data);
    }
    else
    {
      SSL_protocol(data);
    }
    action = wait;
    data = "";
  }
  else if (action == newHeader)
  {
    commandReceived = true;
    if (data != "")
    {
      String nameHeader = data.substring(0, data.indexOf(','));
      String valueHeader = data.substring(data.indexOf(',') + 1, data.length());
      wifi.Add_header(nameHeader, valueHeader);
      action = wait;
      result = "OK";
      data = "";
    }
    else
    {
      result = "ERROR";
    }
  }
  else if (action == tokenAuthorization)
  {
    commandReceived = true;
    if (data != "")
    {
      wifi.Set_token(data);
      action = wait;
      result = "OK";
      data = "";
    }
    else
    {
      result = "ERROR";
    }
  }
  else if (action == ftpTransfer)
  {
    FTP_protocol(data);
    action = wait;
    data = "";
  }
  else if (action == program)
  {
    Program_protocol(data);
    data = "";
  }
  else if (action == resetU)
  {
    Reset_jaco(true);
  }
  else if (action == dResetU)
  {
    Reset_jaco(false);
  }
  else if (action == insecure)
  {
    commandReceived = true;
    if (data != "?" && data != "ON" && data != "OFF")
    {
      result = "ERROR";
    }
    else if (data != "?")
    {
      result = Set_status_ssl(data);
    }
    else
    {
      result = Check_status_ssl();
    }
    data = "";
    action = wait;
  }
}

String Check_status_ssl(String _data)
{
  String cadena = "";
  LittleFS.begin();

  if (LittleFS.exists(STATUS_SSL))
  {
    File infile = LittleFS.open(STATUS_SSL, "r");
    if (infile)
    {
      while (infile.available())
      {
        char c = infile.read();
        cadena += c;
      }
    }
    else
    {
      infile.close();
      cadena = Set_status_ssl(_data);
    }
    infile.close();
  }
  else
  {
    cadena = Set_status_ssl(_data);
  }
  if (cadena != "")
  {
    return cadena;
  }
  else
  {
    return Set_status_ssl(_data);
  }
}

String Check_status_ssl()
{
  String cadena = "";
  LittleFS.begin();

  if (LittleFS.exists(STATUS_SSL))
  {
    File infile = LittleFS.open(STATUS_SSL, "r");
    if (infile)
    {
      while (infile.available())
      {
        char c = infile.read();
        cadena += c;
      }
    }
    else
    {
      infile.close();
      cadena = Set_status_ssl("ON");
    }
    infile.close();
  }
  else
  {
    cadena = Set_status_ssl("ON");
  }
  if (cadena != "")
  {
    return cadena;
  }
  else
  {
    return Set_status_ssl("ON");
  }
}

String Set_status_ssl(String _data)
{
  LittleFS.begin();
  String response = "";
  File infile = LittleFS.open(STATUS_SSL, "w");
  if (infile)
  {
    infile.print("SSL_");
    infile.print(_data);
  }
  infile.close();
  response = Check_status_ssl(_data);
  return response;
}

void Program_protocol(String _data)
{
  if (LittleFS.begin())
  {
    if (LittleFS.exists(_data))
    {
      File infile = LittleFS.open(_data, "r");
      if (infile.size() > 0)
      {
        infile.close();
        Reset_jaco(false);
        delay(500);
        if (samba.connect(BAUD_RATE_PROGRAMER))
        {
          Serial.println("Ingrese a samba.connect...");
          Set_status_micro(false);
          Device device(samba);
          device.create();
          Device::FlashPtr &flash = device.getFlash();
          BossaObserver observer;
          Flasher flasher(samba, device, observer);
          FlasherInfo info;
          flasher.info(info);
          info.print();
          flasher.write(_data.c_str(), OFF_SET);
          Set_status_micro(true);
          LittleFS.rename(_data, RECOVERY);
          action = show;
          Serial.println("Done... ");
          delay(1000);
          Reset_jaco(true);
        }
        else
        {
          Serial.println("No ingrese a samba.connect... ");
          action = show;
          Reset_jaco(true);
        }
      }
      else
      {
        Serial.print("ArchivoSize <= 0: ");
        Serial.println(_data);
        action = show;
        Reset_jaco(true);
      }
    }
    else
    {
      Serial.print("No existe el archivo: ");
      Serial.println(_data);
      Set_status_micro(true);
      action = show;
      Reset_jaco(true);
    }
  }
}

void Set_status_micro(bool status)
{
  LittleFS.begin();
  if (LittleFS.exists(STATUS_MICRO))
  {
    LittleFS.remove(STATUS_MICRO);
  }
  File infile = LittleFS.open(STATUS_MICRO, "w");
  if (infile)
  {
    infile.print(status);
  }
  infile.close();
}

bool Check_status_micro()
{
  bool response = false;
  String cadena = "";
  LittleFS.begin();

  if (LittleFS.exists(STATUS_MICRO))
  {
    File infile = LittleFS.open(STATUS_MICRO, "r");
    if (infile)
    {
      while (infile.available())
      {
        char c = infile.read();
        cadena += c;
      }
      if (cadena.toInt() == 1)
      {
        response = true;
      }
      else
      {
        response = false;
      }
    }
    else
    {
      response = true;
    }
    infile.close();
  }
  else
  {
    response = true;
  }

  return response;
}

void Reset_jaco(bool singleReset)
{
  Serial.println("Reset...");
  pinMode(PIN_RESET_MICRO, OUTPUT);
  if (singleReset)
  {
    Serial.println("Single Reset...");
    delay(500);
    digitalWrite(PIN_RESET_MICRO, LOW);
    delay(800);
    digitalWrite(PIN_RESET_MICRO, HIGH);
    delay(500);
  }
  else
  {
    Serial.println("Double Reset...");
    //
    delay(500);
    digitalWrite(PIN_RESET_MICRO, LOW);
    delay(500);
    digitalWrite(PIN_RESET_MICRO, HIGH);
    delay(200);
    digitalWrite(PIN_RESET_MICRO, LOW);
    delay(500);
    digitalWrite(PIN_RESET_MICRO, HIGH);
    delay(500);
  }
  Serial.println("Reseted...");
}

void FTP_protocol(String _data)
{
  commandReceived = true;
  if (_data != "")
  {
    String server = tools.Get_val_from_json(_data, "server");
    String user = tools.Get_val_from_json(_data, "user");
    String psw = tools.Get_val_from_json(_data, "psw");
    String path = tools.Get_val_from_json(_data, "path");
    ;
    String version = tools.Get_val_from_json(_data, "version");
    String peso = tools.Get_val_from_json(_data, "peso");
    unsigned long pesoBytes = strtoul(peso.c_str(), NULL, 10);
    Serial.print("peso string: ");
    Serial.println(peso);
    Serial.print("server: ");
    Serial.println(server);
    Serial.print("user: ");
    Serial.println(user);
    Serial.print("psw: ");
    Serial.println(psw);
    Serial.print("path: ");
    Serial.println(path);
    Serial.print("version: ");
    Serial.println(version);
    Serial.print("peso: ");
    Serial.println(pesoBytes);

    if (ftp.Start_transfer(user, psw, server, path))
    {
      delay(10);
      File infile = LittleFS.open(path, "r");
      if (infile.size() == pesoBytes)
      {
        infile.close();
        Serial.println("Archivo completo...");
        result = "OK";
      }
      else
      {
        Serial.println("Error abriendo archivo...");
        infile.close();
        result = "ERROR";
      }
    }
    else
    {
      Serial.println("Error descargando archivo...");
      result = "ERROR";
    }
  }
  else
  {
    Serial.println("Error no data...");
    result = "ERROR";
  }
}

void Add_wifi(String _data)
{
  commandReceived = true;

  if (_data != "")
  {
    String nData = _data;
    String ssid = "\"ssid\":";
    String psw = "\"psw\":";
    int cont = 0;
    int index;

    while (_data.indexOf(ssid) != -1)
    {
      _data.remove(0, _data.indexOf(ssid) + ssid.length());
      cont++;
    }
    String ssidVect[cont], pswVect[cont];
    for (int i = 0; nData.indexOf(ssid) != -1 && i < cont; i++)
    {
      index = nData.indexOf('"', nData.indexOf(ssid) + ssid.length());
      nData.remove(0, index + 1);
      index = nData.indexOf('"');
      ssidVect[i] = nData.substring(0, index);
      nData.remove(0, index);
      index = nData.indexOf('"', nData.indexOf(psw) + psw.length());
      nData.remove(0, index + 1);
      index = nData.indexOf('"');
      pswVect[i] = nData.substring(0, index);
      nData.remove(0, index);
    }
    if (wifi.Add_AP(ssidVect, pswVect, cont))
    {
      result = "OK";
    }
    else
    {
      result = "ERROR";
    }
  }
  else
  {
    result = "ERROR";
  }
}

void Connect_to_wifi(String _data)
{
  commandReceived = true;
  if (_data != "")
  {
    Add_wifi(_data);
  }

  if (wifi.Connect_to_AP())
  {
    result = "OK";
  }
  else
  {
    result = "ERROR";
  }
}

void SSL_protocol(String _data)
{
  commandReceived = true;

  if (_data != "")
  {
    String url = "https://";
    url += tools.Get_val_from_json(_data, "url");
    String tipo = tools.Get_val_from_json(_data, "tipo");
    size_t size = tools.Get_val_from_json(_data, "size").toInt();
   
    int index2 = _data.indexOf('$'); // Separador para fingerprint
    String fingerprint = "";
    
    if (index2 != -1)
    {
      fingerprint = _data.substring(index2 + 1, _data.length());
    }
    else
    {
      fingerprint = "";
    }
    
    result = wifi.Sync_data(url, size, fingerprint, tipo);
  }
  else
  {
    result = "ERROR";
  }
}

void No_SSL_protocol(String _data)
{
  commandReceived = true;

  if (_data != "")
  {
    String url = "http://";
    url += tools.Get_val_from_json(_data, "url");
    String tipo = tools.Get_val_from_json(_data, "tipo");
    size_t size = tools.Get_val_from_json(_data, "size").toInt();

    int index2 = _data.indexOf('$'); // Separador para fingerprint
    String fingerprint = "";

    if (index2 != -1)
    {
      fingerprint = _data.substring(index2 + 1, _data.length());
    }
    else 
    {
      fingerprint = "";
    }
    result = wifi.Sync_data(url, size, tipo);
  }
  else
  {
    result = "ERROR";
  }
}

void hw_wdt_disable()
{
  *((volatile uint32_t *)0x60000900) &= ~(1); // Hardware WDT OFF
}

void hw_wdt_enable()
{
  *((volatile uint32_t *)0x60000900) |= 1; // Hardware WDT ON
}
